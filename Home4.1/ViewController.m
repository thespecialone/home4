//
//  ViewController.m
//  Home4.1
//
//  Created by Admin on 28.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "ViewController.h"
#import "CustomView.h"
#import "EditViewController.h"
#import "PersonalDataSource.h"

@interface ViewController () <PersonalDataSource>

@property (nonatomic, strong) CustomView *customView;
@property (nonatomic, strong) PersonalModel *model;

@end

@implementation ViewController

-(instancetype)initWithModel:(PersonalModel *)model{
    self = [super init];
    if (self){
        _model=model;
    }
    return self;
}

-(NSString*)getDataStringByKey:(Key)key{
    switch (key) {
        case currentFirstName:
            return _model.currentFirstName;
        case currentMiddleName:
            return _model.currentMiddleName;
        case currentLastName:
            return _model.currentLastName;
        case currentMail:
            return _model.currentMail;
        case currentPhone:
            return _model.currentPhone;
        case currentCountry:
            return _model.currentCountry;
        default:
            return NULL;
    }
}

-(UIImage*)getPhoto{
    return _model.currentPhoto;
}

- (void)loadView {
    self.customView = [[CustomView alloc] init];
    self.customView.dataSource = self;
    [self setView:self.customView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.customView.editButton addTarget:self action:@selector(callEdit) forControlEvents:UIControlEventTouchUpInside];
   
}

-(void)viewWillAppear:(BOOL)animated{
    [self.customView reloadData];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

-(void) callEdit{
    EditViewController *editController = [[EditViewController alloc] initWithModel:_model];
    [self.navigationController pushViewController:editController animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    }




@end
