//
//  EditVieW.m
//  Home4.1
//
//  Created by Admin on 28.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "EditView.h"

//#import "ViewController.h"


@implementation EditView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        
        
        _photoButton = [[UIButton alloc] init];
        [self addSubview:_photoButton];
        
        _firstnameLabel = [[UILabel alloc] init];
        [_firstnameLabel setText:@"First name:"];
        [self addSubview:_firstnameLabel];
        
        _middlenameLabel = [[UILabel alloc] init];
        [_middlenameLabel setText:@"Middle name:"];
        [self addSubview:_middlenameLabel];
        
        _lastnameLabel = [[UILabel alloc] init];
        [_lastnameLabel setText:@"Last name:"];
        [self addSubview:_lastnameLabel];
        
        _mailLabel = [[UILabel alloc] init];
        [_mailLabel setText:@"E-mail:"];
        [self addSubview:_mailLabel];
        
        _countryLabel = [[UILabel alloc] init];
        [_countryLabel setText:@"Country:"];
        [self addSubview:_countryLabel];
        
        _phonenumberLabel = [[UILabel alloc] init];
        [_phonenumberLabel setText:@"Phone:"];
        [self addSubview:_phonenumberLabel];
        
        
        _changeButton = [[UIButton alloc] init];
        _changeButton.layer.borderWidth = 0.5f;
        _changeButton.layer.cornerRadius = 5.0f;
        _changeButton.layer.borderColor = [UIColor grayColor].CGColor;
        [_changeButton setTitleColor:[UIColor blackColor] forState: UIControlStateNormal];
        [_changeButton setTitle:@"Change" forState:UIControlStateNormal];
        [self addSubview:_changeButton];
        
        _phonenumberLabel = [[UILabel alloc] init];
        [_phonenumberLabel setText:@"Phone:"];
        [self addSubview:_phonenumberLabel];
        
        _firstnameTextField = [[UITextField alloc] init];
        _firstnameTextField.borderStyle = UITextBorderStyleRoundedRect;
        [self addSubview:_firstnameTextField];
        
        _middlenameTextField = [[UITextField alloc] init];
        _middlenameTextField.borderStyle = UITextBorderStyleRoundedRect;
        [self addSubview:_middlenameTextField];
        
        _lastnameTextField = [[UITextField alloc] init];
        _lastnameTextField.borderStyle = UITextBorderStyleRoundedRect;
        [self addSubview:_lastnameTextField];
        
        _mailTextField = [[UITextField alloc] init];
        _mailTextField.borderStyle = UITextBorderStyleRoundedRect;
        [self addSubview:_mailTextField];
        
        _phonenumberTextField = [[UITextField alloc] init];
        _phonenumberTextField.borderStyle = UITextBorderStyleRoundedRect;
        [self addSubview:_phonenumberTextField];
        
        _currentCountryLabel = [[UILabel alloc] init];
        [self addSubview:_currentCountryLabel];
        
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    
    CGFloat offset = 55.0f;
    self.photoButton.frame = CGRectMake((self.frame.size.width - self.photoButton.frame.size.width)/2, offset+44.0f, 150.0f, 150.0f);
    self.photoButton.backgroundColor = [UIColor greenColor];
    self.firstnameLabel.frame = CGRectMake(offset, self.photoButton.frame.origin.y+self.photoButton.frame.size.height+offset, 100.0f, 20.0f);
    self.middlenameLabel.frame = CGRectMake(self.firstnameLabel.frame.origin.x , self.firstnameLabel.frame.origin.y + offset, 110.0f, 20.0f);
    self.lastnameLabel.frame = CGRectMake(self.middlenameLabel.frame.origin.x , self.middlenameLabel.frame.origin.y + offset, 100.0f, 20.0f);
    self.mailLabel.frame = CGRectMake(offset, self.lastnameLabel.frame.origin.y + offset, 60.0f, 20.0f);
    self.phonenumberLabel.frame = CGRectMake(offset, self.mailLabel.frame.origin.y + offset, 60.0f, 20.0f);
    self.countryLabel.frame = CGRectMake(offset, self.phonenumberLabel.frame.origin.y + offset, 70.0f, 20.0f);
    
    self.firstnameTextField.frame = CGRectMake(self.middlenameLabel.frame.origin.x+self.middlenameLabel.frame.size.width+offset, self.firstnameLabel.frame.origin.y, 150.0f, 20.0f);
    self.firstnameTextField.text = [_dataSource getDataStringByKey:currentFirstName];
    
    self.middlenameTextField.frame = CGRectMake(self.middlenameLabel.frame.origin.x+self.middlenameLabel.frame.size.width+offset, self.middlenameLabel.frame.origin.y, 150.0f, 20.0f);
    self.middlenameTextField.text = [_dataSource getDataStringByKey:currentMiddleName];
    
    self.lastnameTextField.frame = CGRectMake(self.middlenameLabel.frame.origin.x+self.middlenameLabel.frame.size.width+offset, self.lastnameLabel.frame.origin.y, 150.0f, 20.0f);
    self.lastnameTextField.text = [_dataSource getDataStringByKey:currentLastName];
    
    self.mailTextField.frame = CGRectMake(self.middlenameLabel.frame.origin.x+self.middlenameLabel.frame.size.width+offset, self.mailLabel.frame.origin.y, 150.0f, 20.0f);
    self.mailTextField.text = [_dataSource getDataStringByKey:currentMail];
    
    self.phonenumberTextField.frame = CGRectMake(self.middlenameLabel.frame.origin.x+self.middlenameLabel.frame.size.width+offset, self.phonenumberLabel.frame.origin.y, 150.0f, 20.0f);
    self.phonenumberTextField.text = [_dataSource getDataStringByKey:currentPhone];
    
    self.currentCountryLabel.frame = CGRectMake(self.countryLabel.frame.origin.x+self.countryLabel.frame.size.width+offset, self.countryLabel.frame.origin.y, 100.0f, 20.0f);
    
    self.changeButton.frame = CGRectMake(self.firstnameTextField.frame.origin.x + self.firstnameTextField.frame.size.width - self.changeButton.frame.size.width, self.countryLabel.frame.origin.y, 70.0f, 20.0f);
    
}

- (void) reloadData{
    self.currentCountryLabel.text = [_dataSource getDataStringByKey:currentCountry];
    [_photoButton setBackgroundImage:[_dataSource getPhoto] forState:(UIControlStateNormal)];
}



@end
