//
//  ImageViewController.m
//  Home4.1
//
//  Created by Admin on 28.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "ImageViewController.h"
#import "ImageCollectionViewCell.h"
#import "ImageModel.h"
#import <Photos/Photos.h>

NSString * const ImageViewControllerCellRuseIdentifier = @"ImageViewControllerCellRuseIdentifier";

@interface ImageViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, ImageModelDelegate>

@property (nonatomic, strong) UICollectionViewFlowLayout *flowLayout;
@property (nonatomic, strong) UICollectionView *coolectionView;
@property (nonatomic, strong) ImageModel *model;
@property (nonatomic, strong) PersonalModel *personalModel;

@end

@implementation ImageViewController

-(instancetype)initWithModel:(PersonalModel *)model{
    self = [super init];
    if (self) {
        _personalModel=model;
    }
    return self;
}

- (void)loadView {
    self.flowLayout = [[UICollectionViewFlowLayout alloc] init];
    self.coolectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:self.flowLayout];
    self.view = self.coolectionView;
}

-(void)didUpdateData{
    [self.coolectionView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.model = [[ImageModel alloc] init];
    
    self.flowLayout.itemSize = CGSizeMake(100.0f, 100.0f);
    self.flowLayout.minimumLineSpacing = 15.0f;
    self.flowLayout.minimumInteritemSpacing = 15.0f;
    self.flowLayout.sectionInset = UIEdgeInsetsMake(15.0, 15.0f, 15.0f, 15.0f);
    
    self.coolectionView.backgroundColor = [UIColor whiteColor];
    self.coolectionView.dataSource = self;
    self.coolectionView.delegate = self;
    self.model.delegate = self;
    
    [self.coolectionView registerClass:[ImageCollectionViewCell class] forCellWithReuseIdentifier:ImageViewControllerCellRuseIdentifier];
    
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return [self.model sectionsCount];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.model countForSection:section];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:ImageViewControllerCellRuseIdentifier forIndexPath:indexPath];
    
    id item = [self.model itemForIndexPath:indexPath];
    
    if ([item isKindOfClass:[PHAsset class]]) {
        PHAsset *asset = (PHAsset *)item;
        [[PHImageManager defaultManager] requestImageForAsset:asset
                                                   targetSize:CGSizeMake(150.0f, 150.f)
                                                  contentMode:PHImageContentModeDefault
                                                      options:nil
                                                resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
                                                    cell.imageView.image = result;
                                                }];
    }
    
    if ([item isKindOfClass:[NSString class]]) {
        NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
        NSString *lastPathComponent = item;
        cell.imageView.image = [UIImage imageWithContentsOfFile:[path stringByAppendingPathComponent:lastPathComponent]];
        
    }
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    id item = self.model.fetchResult[indexPath.item];
    
    if ([item isKindOfClass:[PHAsset class]]) {
        PHAsset *asset = (PHAsset *)item;
        [[PHImageManager defaultManager] requestImageForAsset:asset
                                                   targetSize:CGSizeMake(150.0f, 150.f)
                                                  contentMode:PHImageContentModeDefault
                                                      options:nil
                                                resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
                                                    _personalModel.currentPhoto= result;
                                                }];
    }
    [self.navigationController popViewControllerAnimated:YES];
}


@end
