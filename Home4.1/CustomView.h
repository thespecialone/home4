//
//  CustomView.h
//  Home4.1
//
//  Created by Admin on 28.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//



#import <UIKit/UIKit.h>
#import "PersonalDataSource.h"

@interface CustomView : UIView

@property (nonatomic, strong) UIButton *iconButton;

@property (nonatomic, strong) UILabel *firstnameLabel;
@property (nonatomic, strong) UILabel *middlenameLabel;
@property (nonatomic, strong) UILabel *lastnameLabel;
@property (nonatomic, strong) UIImageView *photo;
@property (nonatomic, strong) UILabel *mailLabel;
@property (nonatomic, strong) UILabel *phonenumberLabel;
@property (nonatomic, strong) UILabel *countryLabel;
@property (nonatomic,strong) UIButton *editButton;
@property (nonatomic, assign) CGFloat topOffset;
@property (nonatomic, weak) id<PersonalDataSource> dataSource;



-(void)reloadData;

@end
