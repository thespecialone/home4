//
//  EditVieW.h
//  Home4.1
//
//  Created by Admin on 28.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PersonalDataSource.h"

@interface EditView : UIView

@property (nonatomic, weak) id<PersonalDataSource> dataSource;
@property (nonatomic, strong) UILabel *firstnameLabel;
@property (nonatomic, strong) UILabel *middlenameLabel;
@property (nonatomic, strong) UILabel *lastnameLabel;
@property (nonatomic, strong) UIButton *photoButton;
@property (nonatomic, strong) UILabel *mailLabel;
@property (nonatomic, strong) UILabel *phonenumberLabel;
@property (nonatomic, strong) UILabel *countryLabel;
@property (nonatomic, strong) UILabel *currentCountryLabel;
@property (nonatomic,strong) UIButton *changeButton;
@property (nonatomic, strong) UITextField *firstnameTextField;
@property (nonatomic, strong) UITextField *middlenameTextField;
@property (nonatomic, strong) UITextField *lastnameTextField;
@property (nonatomic, strong) UITextField *mailTextField;
@property (nonatomic, strong) UITextField *phonenumberTextField;


- (void) reloadData;
@end
