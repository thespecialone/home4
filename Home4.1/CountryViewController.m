//
//  CountryViewComtroller.m
//  Home4.1
//
//  Created by Admin on 28.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "CountryViewController.h"
#import "CountryView.h"
#import "CountryModel.h"
#import "EditViewController.h"
#import "PersonalDataSource.h"

@interface CountryViewController () <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@property (nonatomic, strong) CountryView *countryView;
@property (nonatomic, strong) CountryModel *countryModel;
@property (nonatomic, strong) PersonalModel *model;
@property (nonatomic, strong) UISearchController *searchController;

@end

@implementation CountryViewController

-(instancetype)initWithModel:(PersonalModel *)model{
    self = [super init];
    if (self) {
        _model=model;
    }
    return self;
}

- (void)loadView {
    self.countryView = [[CountryView alloc] init];
    [self setView:self.countryView];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.countryModel = [[CountryModel alloc] init];
    _filteredContent = [[NSMutableArray alloc] init];
    self.searchController = [[UISearchController alloc] init];
    self.countryView.table.dataSource = self;
    self.countryView.table.delegate = self;
    self.countryView.backgroundColor = [UIColor whiteColor];
    
    _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0.0, 44.0, self.countryView.frame.size.width,35.0)];
    self.countryView.table.tableHeaderView = _searchBar;
    self.searchBar.delegate = self;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    if (_isSearching) {
        return [_filteredContent count];
    }
    else {
        return self.countryModel.countries.count;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MYCell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MYCell"];
    }
    
    if (_isSearching) {
        cell.textLabel.text = [_filteredContent objectAtIndex:indexPath.row];
    }
    else {
        cell.textLabel.text = [self.countryModel.countries objectAtIndex:indexPath.row];
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_isSearching) {
        self.model.currentCountry = [self.filteredContent objectAtIndex:indexPath.row];
    } else {
        self.model.currentCountry = [self.countryModel.countries objectAtIndex:indexPath.row];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)searchTableList {
    NSString *searchString = _searchBar.text;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self BEGINSWITH %@", searchString];
    NSArray *result = [_countryModel.countries filteredArrayUsingPredicate:predicate];
    if (result){
        [_filteredContent addObjectsFromArray:result];
    }
}

#pragma mark - Search Implementation

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    _isSearching = YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSLog(@"Text change - %d",_isSearching);
    

    [_filteredContent removeAllObjects];
    
    if([searchText length] != 0) {
        _isSearching = YES;
        [self searchTableList];
    }
    else {
        _isSearching = NO;
    }
    [self.countryView.table reloadData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"Cancel clicked");
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"Search Clicked");
    [self searchTableList];
}

@end
