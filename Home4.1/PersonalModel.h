//
//  PersonalModel.h
//  Home4.1
//
//  Created by Admin on 28.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface PersonalModel : NSObject



@property (nonatomic, strong) UIImage *currentPhoto;
@property (nonatomic, strong) NSString *currentFirstName;
@property (nonatomic, strong) NSString *currentMiddleName;
@property (nonatomic, strong) NSString *currentLastName;
@property (nonatomic, strong) NSString *currentMail;
@property (nonatomic, strong) NSString *currentPhone;
@property (nonatomic, strong) NSString *currentCountry;

- (void)save:(NSObject*)items;
- (void)saveImage:(UIImage *)image;
@end

