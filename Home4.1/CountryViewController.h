//
//  CountryViewComtroller.h
//  Home4.1
//
//  Created by Admin on 28.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PersonalModel.h"

@interface CountryViewController : UIViewController

@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UISearchController *searchBarController;
@property (strong, nonatomic) NSMutableArray *filteredContent;
@property (nonatomic) BOOL isSearching;

-(instancetype)initWithModel:(PersonalModel *)model;

@end

