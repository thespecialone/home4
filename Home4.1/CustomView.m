//
//  CustomView.m
//  Home4.1
//
//  Created by Admin on 28.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "CustomView.h"

@implementation CustomView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        _photo = [[UIImageView alloc] init];
       
        [self addSubview:_photo];
        
        _firstnameLabel = [[UILabel alloc] init];
        [self addSubview:_firstnameLabel];
        
        _middlenameLabel = [[UILabel alloc] init];
        [self addSubview:_middlenameLabel];
        
        _lastnameLabel = [[UILabel alloc] init];
        [self addSubview:_lastnameLabel];
        
        _mailLabel = [[UILabel alloc] init];
        [self addSubview:_mailLabel];
        
        _countryLabel = [[UILabel alloc] init];
        [self addSubview:_countryLabel];
        
        _phonenumberLabel = [[UILabel alloc] init];
        [self addSubview:_phonenumberLabel];
        
        
        _editButton = [[UIButton alloc] init];
        _editButton.layer.borderWidth = 0.5f;
        _editButton.layer.cornerRadius = 5.0f;
        _editButton.layer.borderColor = [UIColor grayColor].CGColor;
        [_editButton setTitleColor:[UIColor blackColor] forState: UIControlStateNormal];
        [_editButton setTitle:@"EDIT" forState:UIControlStateNormal];
        [self addSubview:_editButton];
        
        
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    
    CGFloat offset = 50.0f;
    self.photo.frame = CGRectMake(offset, 2*offset, 150.0f, 150.0f);
    self.photo.backgroundColor = [UIColor greenColor];
    self.firstnameLabel.frame = CGRectMake(self.photo.frame.origin.x + self.photo.frame.size.width + offset, offset, 100.0f, 100.0f);
    self.middlenameLabel.frame = CGRectMake(self.firstnameLabel.frame.origin.x , self.firstnameLabel.frame.origin.y + 2*offset, 150.0f, 100.0f);
    self.lastnameLabel.frame = CGRectMake(self.middlenameLabel.frame.origin.x , self.middlenameLabel.frame.origin.y + 2*offset, 100.0f, 100.0f);
    self.mailLabel.frame = CGRectMake(offset, self.photo.frame.origin.y+self.photo.frame.size.height, 200.0f, 100.0f);
    self.phonenumberLabel.frame = CGRectMake(offset, self.mailLabel.frame.origin.y + 2*offset, 200.0f, 100.0f);
    self.countryLabel.frame = CGRectMake(offset, self.phonenumberLabel.frame.origin.y + 2*offset, 200.0f, 100.0f);
    
    self.editButton.frame = CGRectMake((self.frame.size.width - self.editButton.frame.size.width)/2, self.frame.size.height - 2*offset - self.editButton.frame.size.height, 100.0f, 50.0f);
}

- (void) reloadData{
    self.firstnameLabel.text = [_dataSource getDataStringByKey:currentFirstName];
    self.middlenameLabel.text = [_dataSource getDataStringByKey:currentMiddleName];
    self.lastnameLabel.text = [_dataSource getDataStringByKey:currentLastName];
    self.mailLabel.text = [_dataSource getDataStringByKey:currentMail];
    self.phonenumberLabel.text = [_dataSource getDataStringByKey:currentPhone];
    self.countryLabel.text = [_dataSource getDataStringByKey:currentCountry];
    self.photo.image = [_dataSource getPhoto];
}

- (void)setTopOffset:(CGFloat)topOffset {
    if (_topOffset != topOffset) {
        _topOffset = topOffset;
        [self setNeedsLayout];
    }
}


@end
