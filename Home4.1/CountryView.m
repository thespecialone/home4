//
//  CountryView.m
//  Home4.1
//
//  Created by Admin on 28.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//



#import "CountryView.h"

@implementation CountryView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor greenColor];
        _table = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        [self addSubview:_table];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.table.frame = self.bounds;
}

@end
