//
//  CountryModel.m
//  Home4.1
//
//  Created by Admin on 28.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "CountryModel.h"

@implementation CountryModel

- (instancetype)init {
    self = [super init];
    if (self) {
        _countries = [[NSMutableArray alloc] init];
        NSLocale *currentLocale = [NSLocale currentLocale];
        for(NSString *code in [NSLocale ISOCountryCodes]) {
            NSString *localizedCountry = [currentLocale localizedStringForCountryCode:code];
            if (localizedCountry) {
                [_countries addObject:localizedCountry];
            }
        };
        
    }
    return self;
}

@end
