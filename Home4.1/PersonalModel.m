//
//  PersonalModel.m
//  Home4.1
//
//  Created by Admin on 28.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "PersonalModel.h"

@implementation PersonalModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
        NSData *data = [NSData dataWithContentsOfFile:[path stringByAppendingPathComponent:@"MyFile"]];
        if (data) {
            self = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            [self loadPhoto];
        } else {
            
            _currentFirstName=@"Enter first name ";
            _currentMiddleName=@"Enter middle name";
            _currentLastName=@"Enter last name";
            _currentMail=@"Enter your mail";
            _currentPhone=@"Enter your phone";
            _currentCountry=@"Russia";
          _currentPhoto = [UIImage imageNamed:@"Personal"];
        }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.currentFirstName forKey:@"firstName"];
    [aCoder encodeObject:self.currentMiddleName forKey:@"middleName"];
    [aCoder encodeObject:self.currentLastName forKey:@"lastName"];
    [aCoder encodeObject:self.currentMail forKey:@"mail"];
    [aCoder encodeObject:self.currentPhone forKey:@"phone"];
    [aCoder encodeObject:self.currentCountry forKey:@"country"];
}

- (nullable instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        _currentFirstName = [aDecoder decodeObjectForKey:@"firstName"];
        _currentMiddleName = [aDecoder decodeObjectForKey:@"middleName"];
        _currentLastName = [aDecoder decodeObjectForKey:@"lastName"];
        _currentMail = [aDecoder decodeObjectForKey:@"mail"];
        _currentPhone = [aDecoder decodeObjectForKey:@"phone"];
        _currentCountry = [aDecoder decodeObjectForKey:@"country"];
    }
    return self;
}

-(void)save:(NSObject*)items{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:items];
    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    [data writeToFile:[path stringByAppendingPathComponent:@"MyFile"] atomically:YES];
}

- (void)saveImage:(UIImage *)image {
    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *imageName = @"photo";
    [UIImagePNGRepresentation(image) writeToFile:[path stringByAppendingPathComponent:imageName] atomically:YES];
}

- (void)loadPhoto{
    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *imageName = @"photo";
    self.currentPhoto = [UIImage imageWithContentsOfFile:[path stringByAppendingPathComponent:imageName]];
}
@end



