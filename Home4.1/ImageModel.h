//
//  ImageModel.h
//  Home4.1
//
//  Created by Admin on 28.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <Photos/Photos.h>

@protocol ImageModelDelegate <NSObject>

- (void)didUpdateData;

@end

@interface ImageModel : NSObject

@property (nonatomic, weak) id<ImageModelDelegate> delegate;
@property (nonatomic, strong) PHFetchResult *fetchResult;

- (NSInteger)sectionsCount;

- (NSInteger)countForSection:(NSInteger)section;

- (id)itemForIndexPath:(NSIndexPath *)indexPath;


@end
