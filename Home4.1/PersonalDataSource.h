//
//  PersonalDataSource.h
//  Home4.1
//
//  Created by Admin on 28.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, Key){
    currentFirstName,
    currentMiddleName,
    currentLastName,
    currentMail,
    currentPhone,
    currentCountry,
};

@protocol PersonalDataSource <NSObject>

-(NSString*)getDataStringByKey:(Key)key;
-(UIImage*)getPhoto;

@end
