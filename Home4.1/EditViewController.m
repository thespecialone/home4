//
//  EditViewController.m
//  Home4.1
//
//  Created by Admin on 28.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "EditViewController.h"
#import "EditView.h"
#import "ImageViewController.h"
#import "PersonalDataSource.h"
#import "CountryViewController.h"

@interface EditViewController () <PersonalDataSource>

@property (nonatomic, strong) EditView *editView;
@property (nonatomic, strong) PersonalModel *model;
@property (nonatomic) BOOL corectInputData;

@end

@implementation EditViewController

-(instancetype)initWithModel:(PersonalModel *)model{
    self = [super init];
    if (self) {
        _model=model;
    }
    return self;
}

- (void)loadView {
    self.editView = [[EditView alloc] init];
    self.editView.dataSource = self;
    [self setView:self.editView];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self.editView.photoButton addTarget:self action:@selector(callEditImage) forControlEvents:UIControlEventTouchUpInside];
    [self.editView.changeButton addTarget:self action:@selector(changeCountry) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(checkInputData)];
}

-(void)viewWillAppear:(BOOL)animated{
    [self.editView reloadData];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
}

-(void) callEditImage{
    ImageViewController *imageController = [[ImageViewController alloc] initWithModel:_model];
    [self.navigationController pushViewController:imageController animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

-(NSString*)getDataStringByKey:(Key)key{
    switch (key) {
        case currentFirstName:
            return _model.currentFirstName;
        case currentMiddleName:
            return _model.currentMiddleName;
        case currentLastName:
            return _model.currentLastName;
        case currentMail:
            return _model.currentMail;
        case currentPhone:
            return _model.currentPhone;
        case currentCountry:
            return _model.currentCountry;
        default:
            return NULL;
    }
}

-(UIImage*)getPhoto{
    return _model.currentPhoto;
}

-(void) changeCountry{
    CountryViewController *countryController = [[CountryViewController alloc] initWithModel:_model];
    [self.navigationController pushViewController:countryController animated:YES];
}


-(void)checkInputData{
    
    _corectInputData = YES;
    
        self.editView.firstnameTextField.layer.backgroundColor = [UIColor whiteColor].CGColor;
        self.editView.middlenameTextField.layer.backgroundColor = [UIColor whiteColor].CGColor;
        self.editView.lastnameTextField.layer.backgroundColor = [UIColor whiteColor].CGColor;
  
    
    if (![self validateEmail:self.editView.mailTextField.text]) {
        self.editView.mailTextField.layer.backgroundColor = [UIColor redColor].CGColor;
        _corectInputData = NO;
    } else {
        self.editView.mailTextField.layer.backgroundColor = [UIColor whiteColor].CGColor;
    }
    
    if (![self validatePhone:self.editView.phonenumberTextField.text] || [self.editView.phonenumberTextField.text isEqualToString:@""]) {
        self.editView.phonenumberTextField.layer.backgroundColor = [UIColor redColor].CGColor;
        _corectInputData = NO;
    } else {
        self.editView.phonenumberTextField.layer.backgroundColor = [UIColor whiteColor].CGColor;
    }
    
    if (_corectInputData == YES ) {
        [self saveChanges];
    }
}

- (BOOL)validateEmail:(NSString*) emailString {
    NSString *regExPattern = @"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$";
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    NSLog(@"%lu", (unsigned long)regExMatches);
    if (regExMatches == 0) {
        return NO;
    }
    else
        return YES;
}

-(BOOL)validatePhone:(NSString*)inputString{
    BOOL isValid = NO;
    NSCharacterSet *alphaNumbersSet = [NSCharacterSet decimalDigitCharacterSet];
    NSCharacterSet *stringSet = [NSCharacterSet characterSetWithCharactersInString:inputString];
    isValid = [alphaNumbersSet isSupersetOfSet:stringSet];
    return isValid;
}


-(void)saveChanges{
    _model.currentFirstName = self.editView.firstnameTextField.text;
    _model.currentMiddleName = self.editView.middlenameTextField.text;
    _model.currentLastName = self.editView.lastnameTextField.text;
    _model.currentMail = self.editView.mailTextField.text;
    _model.currentPhone = self.editView.phonenumberTextField.text;
    [self.model save:_model];
    [self.model saveImage:_model.currentPhoto];
    [self.navigationController popViewControllerAnimated:YES];
}
@end
