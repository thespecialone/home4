//
//  CountryView.h
//  Home4.1
//
//  Created by Admin on 28.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

@interface CountryView : UIView

@property (nonatomic, strong) UITableView *table;

@end
