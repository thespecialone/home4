//
//  EditViewController.h
//  Home4.1
//
//  Created by Admin on 28.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PersonalModel.h"

@interface EditViewController : UIViewController


-(instancetype) initWithModel:(PersonalModel*)model;

@end
