//
//  ImageModel.m
//  Home4.1
//
//  Created by Admin on 28.11.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "ImageModel.h"


@interface ImageModel () <PHPhotoLibraryChangeObserver>

@property (nonatomic, strong) NSMutableArray *mutableImages;


@end

@implementation ImageModel

- (instancetype)init {
    self = [super init];
    if (self) {
        [[PHPhotoLibrary sharedPhotoLibrary] registerChangeObserver:self];
        
        _fetchResult = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:nil];
        
        NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
        _mutableImages = [NSKeyedUnarchiver unarchiveObjectWithFile:[path stringByAppendingPathComponent:@"ImageModelItems"]];
        if (!_mutableImages) {
            _mutableImages = [[NSMutableArray alloc] init];
        }
    }
    return self;
}

- (void)photoLibraryDidChange:(PHChange *)changeInstance {
    PHFetchResultChangeDetails *details = [changeInstance changeDetailsForFetchResult:self.fetchResult];
    
    if (details) {
        self.fetchResult = details.fetchResultAfterChanges;
    }
    
    if(self.delegate) {
        [self.delegate didUpdateData];
    }
    
}

- (NSInteger)sectionsCount {
    return self.mutableImages.count > 0 ? 2 : 1;
}

- (NSInteger)countForSection:(NSInteger)section {
    return self.fetchResult.count;
}

- (id)itemForIndexPath:(NSIndexPath *)indexPath {
    return self.fetchResult[indexPath.item];
}


@end
